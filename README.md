# useIsDesktop react hook

Super tiny hook to find out if user agent is desktop or mobile (including tablets).
To achieve the goal it checks the window `navigator` property.

## Install

`yarn add use-is-desktop` or `npm i use-is-desktop`

## Use

```
import useIsDesktop from 'use-is-desktop'

...
// Insede component
const isDesktop = useIsDesktop(false)
...
```

You can pass default value as a hook function parameter (`false` if not specified). After component first render the hook will check the window `navigator` parameter. Before that and in case of `navigator` absence (e.g. while SSR) the hook will return the default value
