import { useEffect, useState } from 'react'

/* eslint-disable no-undef */
function useIsDesktop(defaultVal) {
  const [isDesktop, setIsDesktop] = useState(defaultVal)
  useEffect(() => {
    if (navigator !== 'undefined') {
      setIsDesktop(
        !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(
          navigator.userAgent
        )
      )
    }
  }, [])
  return isDesktop
}

export default useIsDesktop
